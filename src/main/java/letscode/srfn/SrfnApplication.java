package letscode.srfn;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrfnApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrfnApplication.class, args);
	}

}
